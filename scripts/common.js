﻿var runtimeOrExtension = chrome.runtime && chrome.runtime.sendMessage ? 'runtime' : 'extension';

function listen(callback) {
    chrome[runtimeOrExtension].onMessage.addListener(
    function (request, sender, sendResponse) {
        return callback(request, sendResponse);
    });
}

function sendMsg(data, callback) {
    chrome[runtimeOrExtension].sendMessage(data, function (response) {
        if (callback) {
            callback(response);
        }
    })
}

function sendMsgToAllPages(data, callback) {
    chrome.tabs.query({ currentWindow: true }, function (tabs) {
        for (var i = 0; i < tabs.length; i++) {
            chrome.tabs.sendMessage(tabs[i].id, data, function (result) {
                if (callback) callback(result);
            });
        }
    })
}

function sendMsgToCurrentPage(data, callback) {
    chrome.tabs.getSelected(function (tab) {
        chrome.tabs.sendMessage(tab.id, data, function (result) {
            if (callback) callback(result);
        });
    });
}