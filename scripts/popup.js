﻿/// <reference path="common.js" />
var all = {
    enable: localStorage.enable,
    brightness: localStorage.brightness || '115',
    contrast: localStorage.contrast || '70',
    ts: localStorage.ts || '7',
    te: localStorage.te || '7'
};

sendMsgToCurrentPage({ key: 'isenable' }, function (r) {
    if (all.enable == "1")
        $('.switch input[name=aswitch]').eq(0).prop('checked', true);
    else if (all.enable == "-1")
        $('.switch input[name=aswitch]').eq(2).prop('checked', true);
    //    if (r.enable == "1") {
    //        $('.switch input[name=switch]').eq(0).prop('checked', true);
    //    } else if (r.enable == "-1") {
    //        $('.switch input[name=switch]').eq(2).prop('checked', true);
    //    }
    $('.head input[name=child][value=' + r.state + ']').prop('checked', true);
    $('b[t=h]').text(r.origin);
    checkIsTR();
});
$('#c').val(all.contrast);
$('#b').val(all.brightness);
$('#ts').val(all.ts);
$('#te').val(all.te);
$('.switch input[name=aswitch]').each(function (i, r) {
    $(r).click(function () {
        var enable = localStorage.enable = $('.switch input[name=aswitch]:checked').val();
        //$('.switch input[name=switch]').eq(enable == "1" ? 0 : enable == "-1" ? 2 : 1).prop('checked', true);
        sendMsgToAllPages({ key: 'setenable', enable: enable });
        checkIsTR();
    });
})
$('.switch input[name=switch]').each(function (i, r) {
    $(r).click(function () {
        var enable = $('.switch input[name=switch]:checked').val();
        sendMsgToCurrentPage({ key: 'setenable', enable: enable });
        checkIsTR();
    });
})
$('.head input[name=child]').each(function (i, r) {
    $(r).click(function () {
        $('.head input[name=child]').eq(i == 0 ? 1 : 0).prop('checked', false);
        if ($('.head input[name=child]:checked').length > 0) {
            var state = $('.head input[name=child]:checked').val();
            sendMsgToCurrentPage({ key: 'setstate', state: state });
        } else {
            sendMsgToCurrentPage({ key: 'resetstate' });
        }

    });
})
$('input[type=range]').change(function () {
    localStorage.brightness = $('#b').val();
    localStorage.contrast = $('#c').val();
    sendMsgToAllPages({ key: 'setops', brightness: $('#b').val(), contrast: $('#c').val() });
});
$('input[type=number]').change(function () {
    localStorage.ts = $('#ts').val();
    localStorage.te = $('#te').val();
    sendMsgToAllPages({ key: 'setts', ts: $('#ts').val(), te: $('#te').val() });
});

function checkIsTR() {
    if ($('.switch input[name=aswitch]:checked').val() == "-1") { $('#tr').fadeIn(388); }
    else { $('#tr').fadeOut(388); }
}

var langs = {
    'zh-cn': {
        'Night Reading Mode 2.0': '夜间阅读模式2.0',
        'All Pages': '全部页面',
        'ON': '开启',
        'OFF': '关闭',
        'AUTO': '自动',
        'Always be ON for': '总是开启的',
        'Always be OFF for': '总是关闭的',
        'Auto ON from': '自动开启从晚上',
        'pm to tomorrow': '点到明天早上',
        'am': '点',
        'Brightness': '亮度',
        'Contrast': '对比度',
        'If you like the extension, please rate with ★ ★ ★ ★ ★ 5 stars and give it a +1.':
         '如果您喜欢这个插件，请给 ★ ★ ★ ★ ★ 5颗星并且给它一个+1。',
        'Rate it': '评分'
    }
}

$('[g=l]').each(function (i, g) {
    var key = $(g).attr('key');
    var value = key;
    var lang = navigator.language.toLowerCase();
    try {
        if (langs[lang] && langs[lang][key]) {
            value = langs[lang][key];
        }
    } catch (e) { }
    $(g).text(value);
});

$(function () {
    setTimeout(function () {
        $('head').append('<script type="text/javascript" async="true" src="https://apis.google.com/js/plusone.js"></script>');
    }, 10);
});

if (!localStorage.met1) {
    $('#met').show();
}
$('#remove').click(function () { $('#met').remove(); localStorage.met1 = '1'; });