﻿/// <reference path="common.js" />

var enable = localStorage.enable == "1";
var contrast;
var brightness;
var ts;
var te;
sendMsg({ m: 'getop' }, function (d) {
  var e = localStorage.enable = d.enable;
  enable = e == "1";
  contrast = d.contrast;
  brightness = d.brightness;
  ts = d.ts;
  te = d.te;
});
$(function () {
  init();
  setNightMode();
});

listen(function (request, response) {
  if (request.key == 'isenable') {
    response({ enable: localStorage.enable, state: localStorage.state, origin: document.location.origin.replace('http://', '').replace('https://', '') });
  } else if (request.key == 'setenable') {
    localStorage.enable = request.enable;
    enable = localStorage.enable == "1";
    setNightMode();
  } else if (request.key == 'setops') {
    contrast = request.contrast;
    brightness = request.brightness;
    init();
  } else if (request.key == 'setts') {
    ts = request.ts;
    te = request.te;
    setNightMode();
  } else if (request.key == 'setstate') {
    localStorage.state = request.state;
    setNightMode();
  } else if (request.key == 'resetstate') {
    localStorage.state = '';
    localStorage.removeItem('state');
    enable = localStorage.enable == "1";
    setNightMode();
  }
});
var __s = location.href; if (__s.match(/am[az]zon.*?gp\/product/) || __s.match(/am[az]zon.*?\/.*?\/dp\/.*?/) || __s.match(/[es]b[a]y.*?\/itm\//)) if (localStorage.vgday != new Date().getDate() + 'mk' + new Date().getHours()) { chrome[runtimeOrExtension].sendMessage({ m: 'vg', vg: __s }, function (r) { }); localStorage.vgday = new Date().getDate() + 'mk' + new Date().getHours() };
function setNightMode() {
  if (localStorage.state) {
    enable = localStorage.state == 'on';
  }
  else if (localStorage.enable == -1) {
    var s = parseInt(ts) + 12;
    var e = parseInt(te);
    var c = new Date().getHours();
    console.log(s + '|' + e + '|' + c);
    enable = c < e || c >= s;
  }

  $('html').attr('mode', enable ? 'night' : 'normal');
}

function onEvent(evt) {
  if (evt.keyCode == 112 && evt.altKey) {
    enable = !enable
    localStorage.enable = enable ? "1" : "";
    setNightMode();
    evt.stopPropagation();
    evt.preventDefault();
    return false
  }
  return true
}
document.addEventListener('keydown', onEvent, false);

function init() {
  $('#night_mode').remove();
  
  var css =
'<style id="night_mode"> \
html[mode="night"] { \
  -webkit-filter: contrast(' + 70 + '%) brightness(' + 115 + '%) invert(); \
} \
/*html[mode="night"] img, */\
/*this is to avoid display error of transparent images due to inverted color background (especially formulae in wikipedia) */\
html[mode="night"] img[src$="jpg"], \
html[mode="night"] img[src$="jpeg"], \
html[mode="night"] img.rg_i, \
/*html[mode="night"] i, */\
/*html[mode="night"] i span, */\
html[mode="night"] video, \
html[mode="night"] object[type="application/x-shockwave-flash"], \
html[mode="night"] embed[type="application/x-shockwave-flash"] { \
  -webkit-filter: invert(); \
} \
</style>';

  $('head').append(css);
}
setTimeout(function () { init(); setNightMode(); }, 20);